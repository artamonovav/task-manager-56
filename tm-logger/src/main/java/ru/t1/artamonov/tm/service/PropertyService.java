package ru.t1.artamonov.tm.service;

import lombok.SneakyThrows;
import org.apache.activemq.ActiveMQConnection;
import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Service;
import ru.t1.artamonov.tm.api.IPropertyService;

import java.util.Properties;

@Service
public final class PropertyService implements IPropertyService {

    @NotNull
    private static final String EMPTY_VALUE = "---";

    @NotNull
    private static final String FILE_NAME = "application.properties";

    @NotNull
    private static final String MONGO_PORT = "mongo.port";

    @NotNull
    private static final String MONGO_PORT_DEFAULT = "27017";

    @NotNull
    private static final String MONGO_HOST = "mongo.host";

    @NotNull
    private static final String MONGO_HOST_DEFAULT = "localhost";

    @NotNull
    private static final String MONGO_DB_NAME = "mongo.dbname";

    @NotNull
    private static final String MONGO_DB_NAME_DEFAULT = "example";

    @NotNull
    private static final String MQ_BROKER_URL = "broker.url";

    @NotNull
    private static final String MQ_BROKER_URL_DEFAULT = ActiveMQConnection.DEFAULT_BROKER_URL;

    @NotNull
    private static final String MQ_BROKER_QUEUE = "broker.queue";

    @NotNull
    private static final String MQ_BROKER_QUEUE_DEFAULT = "LOGGER";


    @NotNull
    private final Properties properties = new Properties();

    @SneakyThrows
    public PropertyService() {
        properties.load(ClassLoader.getSystemResourceAsStream(FILE_NAME));
    }

    @NotNull
    private String getEnvKey(@NotNull final String key) {
        return key.replace(".", "_").toUpperCase();
    }

    @NotNull
    private String getStringValue(@NotNull final String key, @NotNull final String defaultValue) {
        if (System.getProperties().containsKey(key)) return System.getProperties().getProperty(key);
        @NotNull final String envKey = getEnvKey(key);
        if (System.getenv().containsKey(envKey)) return System.getenv(envKey);
        return properties.getProperty(key, defaultValue);
    }

    @NotNull
    private String getStringValue(@NotNull final String key) {
        return getStringValue(key, EMPTY_VALUE);
    }

    @NotNull
    private Integer getIntegerValue(@NotNull final String key, @NotNull final String defaultValue) {
        return Integer.parseInt(getStringValue(key, defaultValue));
    }

    @NotNull
    @Override
    public String getHostName() {
        return getStringValue(MONGO_HOST, MONGO_HOST_DEFAULT);
    }

    @NotNull
    @Override
    public Integer getPort() {
        return getIntegerValue(MONGO_PORT, MONGO_PORT_DEFAULT);
    }

    @NotNull
    @Override
    public String getDbName() {
        return getStringValue(MONGO_DB_NAME, MONGO_DB_NAME_DEFAULT);
    }

    @NotNull
    @Override
    public String getBrokerUrl() {
        return getStringValue(MQ_BROKER_URL, MQ_BROKER_URL_DEFAULT);
    }

    @NotNull
    @Override
    public String getBrokerQueue() {
        return getStringValue(MQ_BROKER_QUEUE, MQ_BROKER_QUEUE_DEFAULT);
    }

}
