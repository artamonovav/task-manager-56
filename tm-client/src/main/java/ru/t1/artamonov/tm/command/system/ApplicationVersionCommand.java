package ru.t1.artamonov.tm.command.system;

import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;
import ru.t1.artamonov.tm.dto.request.ServerVersionRequest;
import ru.t1.artamonov.tm.dto.response.ServerVersionResponse;

@Component
public final class ApplicationVersionCommand extends AbstractSystemCommand {

    @NotNull
    private static final String NAME = "version";

    @NotNull
    private static final String ARGUMENT = "-v";

    @NotNull
    private static final String DESCRIPTION = "Display program version.";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("[VERSION]");
        System.out.println("[Client]");
        System.out.println(propertyService.getApplicationVersion());
        System.out.println("[Server]");
        @NotNull final ServerVersionRequest request = new ServerVersionRequest(getToken());
        @NotNull final ServerVersionResponse response = serviceLocator.getSystemEndpoint().getVersion(request);
        System.out.println("Client: " + response.getVersion());
    }

}
