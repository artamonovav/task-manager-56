package ru.t1.artamonov.tm.api.repository.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.artamonov.tm.model.Session;

import java.util.List;

public interface ISessionRepository {

    void add(@NotNull Session session);

    void update(@NotNull Session session);

    void remove(@NotNull Session session);

    void removeByUserId(@NotNull String userId);

    @Nullable
    List<Session> findAll();

    @Nullable
    List<Session> findAll(@NotNull String userId);

    @Nullable
    Session findOneById(@NotNull String id);

    @Nullable
    Session findOneByIdUserId(@NotNull String userId, @NotNull String id);

}
