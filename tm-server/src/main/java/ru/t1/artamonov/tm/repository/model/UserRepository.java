package ru.t1.artamonov.tm.repository.model;

import lombok.AllArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import ru.t1.artamonov.tm.api.repository.model.IUserRepository;
import ru.t1.artamonov.tm.model.User;

import javax.persistence.EntityManager;
import java.util.List;

@Repository
@Scope("prototype")
@AllArgsConstructor
public final class UserRepository implements IUserRepository {

    @NotNull
    @Autowired
    private EntityManager entityManager;

    @Override
    public void add(@NotNull User user) {
        entityManager.persist(user);
    }

    @Override
    public @Nullable List<User> findAll() {
        @NotNull final String jpql = "SELECT m FROM User m";
        return entityManager.createQuery(jpql, User.class).getResultList();
    }

    @Override
    public @Nullable User findOneById(@Nullable String id) {
        @NotNull final String jpql = "SELECT m FROM User m WHERE m.id = :id";
        return entityManager.find(User.class, id);
    }

    @Override
    public @Nullable User findByLogin(@Nullable String login) {
        @NotNull final String jpql = "SELECT m FROM User m WHERE m.login = :login";
        return entityManager.createQuery(jpql, User.class)
                .setHint("org.hibernate.cacheable", true)
                .setParameter("login", login)
                .setMaxResults(1).getResultList().stream().findFirst().orElse(null);
    }

    @Override
    public @Nullable User findByEmail(@Nullable String email) {
        @NotNull final String jpql = "SELECT m FROM User m WHERE m.email = :email";
        return entityManager.createQuery(jpql, User.class)
                .setHint("org.hibernate.cacheable", true)
                .setParameter("email", email)
                .setMaxResults(1).getResultList().stream().findFirst().orElse(null);
    }

    @Override
    public void remove(@NotNull User user) {
        entityManager.remove(user);
    }

    @Override
    public void update(@NotNull User user) {
        entityManager.merge(user);
    }

}
